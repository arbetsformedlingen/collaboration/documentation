# FAQ
Brief FAQ:s with links to clarification.

Just stubs for now.

- use temp mount in yaml instead of chgroup in dockerfile                                                                                                                                                                                                                                                                  
- you can have 1 codebase, several deploys with aardvark                                                                                                                                                                                                                                                                   
- how to construct internal routes names?                                                                                                                                                                                                                                                                                  
- dont use low port numbers, because unix                                                                                                                                                                                                                                                                                  
- check that exposed container ports match the srvice ports 

- to browse URLs ending with .jtech.se, you need to add JobTech's root CA to your browser's list of trusted certificates. See this thread for instructions:
https://gitlab.com/arbetsformedlingen/devops/calamari-documentation/-/issues/95

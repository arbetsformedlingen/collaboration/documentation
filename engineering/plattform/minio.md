# Minio

## Administration
The administration URL is https://minio-console.arbetsformedlingen.se/login

Enter valid Minio credentials to login.


## Set a user's policy
```
mc admin policy set local getonlypipeline-dev user=pipeline-dev
```

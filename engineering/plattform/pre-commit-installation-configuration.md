# pre-commit
[pre-commit](https://pre-commit.com/)

A tool that can run configurable commands when code before commited or before it's pushed

## Pre-commit checks
Install pre-commit using your normal dependency manager. 
Since it's not needed at runtime, only when developing, it can be configured as a dev-dependecy if your workflow allows it.

Example from a Python project that uses Poetry:
```yaml
[tool.poetry.group.dev.dependencies]
pre-commit = "^3.6.0"
```

The checks are defined in a file named `.pre-commit-config.yaml` in the root of the repo and you need to install the git hooks with `pre-commit install`.
This installs the code for linters, formatters etc and sets up the pre-commit hooks.
For Python project, configure tools in a file named `pyproject.toml`(even if you don't use it for dependency management)
Time-consuming checks can be done pre-push instead of pre-commit.
If you want to run all checks without committing, the command is: `pre-commit run` for changed files, or `pre-commit run --all-files` for all files.
If you absolutely need to commit code that doesn't pass the checks, add `--no-verify` to the commit command, e.g.
`git commit --no-verify -m "commit message"`

## Example
From a Python project
```yaml
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v3.2.0
    hooks:
      - id: no-commit-to-branch  # default: main, master
      - id: mixed-line-ending
      - id: trailing-whitespace
        name: Remove trailing whitespace (changes files)

  # Linting and formatting with Ruff. Config is in pyproject.toml
  - repo: https://github.com/astral-sh/ruff-pre-commit
    rev: v0.1.9
    hooks:
      # Configuration in pyproject.toml
      - id: ruff
        name: Ruff linter
      - id: ruff-format

```

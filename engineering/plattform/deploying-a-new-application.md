# Deploying a new application at JobTech Dev

So you have a nice application and want to deploy it in an Openshift cluster? No problem!


## Quick explanation of how things are setup right now

### Openshift projects
Openshift projects/namespace are where application deployments live in the cluster.
Namespace creation is automated and the declarations reside in a [a gitopsed configuration](https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra/-/tree/main/kustomize/overlays). Each cluster has its own declaration - click the link and check out our current namespaces!

### Application infra repos
Each application repo needs a sibling repo with the same name, but
with `-infra` appended to the name. It should contain a description of
the deployment.

### Delivery
[ArgoCD](https://argo-cd.readthedocs.io/en/stable/) is a declarative,
gitops continuous delivery tool for Openshift. Its job is to monitor
your applications' infra repos, and when it detects a change, it
applies the infra repo to the cluster. A typical change is when you
want to update the application version in a cluster and enter a new
git-hash in an overlay.

The delivery is automated and the declarations reside in [a gitopsed configuration](https://gitlab.com/arbetsformedlingen/devops/argocd-infra/-/tree/main/kustomize/clusters). Each cluster has its own declaration - click the link and check out our current application deliveries!

### Build and delivery control
To have your application automatically built and images saved in [Nexus](https://nexus.jobtechdev.se/), and to get a convenient way to update the application's infra repo with new versions for the delivery tool to act on, you can use [jobtech-ci](https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci) - a thin layer on [Gitlab CI](https://docs.gitlab.com/ee/ci/).


## Checklist

 - [x] The application has a working Dockerfile in its git repo root directory.
 - [x] You have created an infra repo for your application, named as the application repo but with `-infra` appended to its name.
 - [x] You have followed the instructions [here](https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci) to setup the build and delivery control in your application repo.


## Set up a new application
 0. Edit `namespaces.yaml` and `kustomization.yaml` in [the name space configuration repo](https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra/-/tree/main/kustomize/overlays), in the desired cluster overlay directory, then create a merge request. Ask a member of Calamari to approve. Avoid using the deprecated `test` overlay - use `testing` instead.
 1. Create a new file for your application, and edit `kustomization.yaml` in [the gitopsed delivery control configuration](https://gitlab.com/arbetsformedlingen/devops/argocd-infra/-/tree/main/kustomize/clusters), in the the desired cluster overlay directory, then create a merge request. Ask a member of Calamari to approve. Avoid using the deprecated `test` overlay - use `testing` instead.

## Kludge: IF cluster==onprem-test THEN do this too
This step is needed to prevent permission errors in argocd.

 0. Log in to the onprem-test cluster with `oc` and switch to your project.
 1. Download [this permission file](engineering/plattform/permission.yaml). You can optionally make customisations to this file, for example adding the cron permission.
 2. `oc apply -f permission.yaml`

 Please note that you cannot use Crossplane DNS entries in onprem or onprem-test. Crossplane is not installed there, and the AF firewall would prevent it from working.

## Verify that it works
 0. Log into the Openshift cluster and verify that the namespace has been created.
 1. Trig a build in your application repo, for example by adding a
    newline to some file. Verify that a Gitlab CI pipeline starts and
    finishes successfully.
 2. Log into [Nexus](https://nexus.jobtechdev.se/#browse/browse:JobtechdevDockerRegistry) and browse for your application, and verify that it has a new tag corresponding to your latest app version.
 3. Log into the cluster's [ArgoCD](https://openshift-gitops-server-openshift-gitops.apps.testing.services.jtech.se/applications?showFavorites=false&proj=&sync=&autoSync=&health=&namespace=&cluster=&labels=) and verify that your application is registered in the application list.
 4. Click on the application and monitor the delivery process. Typically, two errors states can occur, 1) ArgoCD has not detected that the infra repo has been updated (the right sync field has the wrong value), or 2) ArgoCD has failed to sync the application's updated infra repo to the cluster namespace (the left sync field has the wrong value). In case there is an error, inspect the events and log tabs.
 5. Log back into the Openshift cluster and verify that your application has been deployed correctly to its namespace.

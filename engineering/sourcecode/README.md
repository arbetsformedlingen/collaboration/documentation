# Source code management

## Fundamentals
All code should be placed under https://gitlab.com/arbetsformedlingen.

Existing code in other locations should be moved here. Ask the devops team for assistance if needed.

All code should be open source by default, i.e. the projects should be public. Special conditions call for exceptions, such as projects containing credentials.

## Security updates
It is important to keep your source code secure and its dependencies up to date.

Use [Renovate](https://gitlab.com/arbetsformedlingen/devops/jobtech-renovate) for automatic dependency updates.


## Managing a credential leak in a git repo

### Primary action

Immediately alert the devops team.

Disable any accounts for which the credentials have leaked.

Revoke any leaked private keys and certificates.

### Secondary action

It is not enough to simply delete the credentials in a file and commit it. After that, the git history needs to be cleaned of any traces of the leak.

The procedure described here been successfully tested:
https://docs.gitlab.com/ee/user/project/repository/reducing_the_repo_size_using_git.html#purge-files-from-repository-history

Follow all instructions in that file meticulously.

For example, to remove a file with leaks from history, use
```
git-filter-repo  --path path/to/file --invert-paths
```

After everything has been done, please verify that everything has been removed by cloning the cleaned repo from Gitlab, and run:
```
git rev-list --all | xargs -L 1 git grep THESECRETYOUREMOVED
```


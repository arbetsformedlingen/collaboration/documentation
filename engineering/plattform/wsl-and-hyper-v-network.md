# WSL and Hyper-V network

Most of the time automatically generated network configurations in WSL and
Hyper-V VMs won't work satisfactorily. The cause, at least partly, seems to
be the VPN solution used by AF. This might apply to other VM solutions
as well.

## WSL and WSL2

For Ubuntu and other Debian-based machines change the configuration of WSL
to not touch the `resolve.conf` file. The `/etc/wsl.conf` file needs to
contain the following.

```ini
[network]
generateResolvConf = false
```

This can be accomplished by running the following command.

```shell
sudo bash -c 'echo "[network]\ngenerateResolvConf = false" > /etc/wsl.conf'
```

The systems DNS server can then be configured manually in `/etc/resolv.conf`
without being overwritten by the automatic system.

First, decide what DNS server you want to use. To reach the internet it is
sufficient to use one of the public ones like `8.8.8.8` (Google Public DNS)
or `1.1.1.1` (CloudFlare). To be able to resolve internal DNS names you need
to use `10.142.6.10` or `10.142.0.70`. When using the internal DNS it will
only work when connected with the VPN.

```ini
nameserver 8.8.8.8
```

This can be accomplished by running the following commands.

```shell
sudo bash -c 'echo "nameserver 8.8.8.8" > /etc/resolv.conf'
```

## Find the DNS server in use

In Windows PowerShell run `Get-DnsClientServerAddress` to get the current DNS
servers configured for all networks.

## Better solutions

It is tempting to enter two name servers into `/etc/resolv.conf`. They will
be used in the entered order. The system tends to be too "sticky" to actually
switch back to use the higher prioritised ones in a timely fashion. A local
DNS server like dnsmasq or Unbound might handle this better.

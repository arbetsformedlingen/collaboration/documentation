# JobTech Documentation collection

## Sub-sections

* [Engineering](engineering/) - intended for engineers within JobTech
* [Engineering - internal information](https://gitlab.com/arbetsformedlingen/devops/calamari-documentation/-/blob/master/README.md?ref_type=heads) - more information
* [Getting started for external users](https://gitlab.com/arbetsformedlingen/collaboration/documentation/-/blob/main/getting_started_external.md)
* more sections to come


## Introduction

Professionell, inspirerande och förtroendeingivande

Arbetsförmedlingens gitlab repository.

## Backlog

Each product and team has its own backlog, the aggregated backlog can be found here:  https://gitlab.com/groups/arbetsformedlingen/-/issues

jobtech links: https://gitlab.com/groups/arbetsformedlingen/-/boards?scope=all&label_name[]=JobTech%20Links

devops(calamari): https://gitlab.com/groups/arbetsformedlingen/-/issues?scope=all&state=opened&label_name[]=Calamari

jobsearch(jobbdata) : https://gitlab.com/groups/arbetsformedlingen/-/boards/6034306?label_name%5B%5D=Team+Jobbdata

## Gitlab CI
[Gitlab CI](https://docs.gitlab.com/ee/ci/) is our prefered build system.

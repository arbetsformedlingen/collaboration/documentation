# Guidelines for Python projects

General info on how to work with Python projects and especially pip/Poetry.
Link to this document from other projects' documentation to avoid repetition.
Expand this document as needed.

## Dependency management

### Specify top level dependencies only

No:

```
requests==version
certifi==version
```

Yes:

```
requests==version
```

certifi is installed by requests, so it does not have to be specified separately. The same principle applies to
Flask/Werkzeug and other dependencies.

## Pip

- Projects that use pip should have a file named `requirements.txt`in the root of the repo.
- If dependencies are specified in other files (e.g. separate dev-dependencies), this must be clearly documented in the
  project.

### Jobtech PyPi with pip

If you are using packages from _Jobtech's PyPi_, some configuration is needed.
A file called `pip.ini` with this specification should be used:

```ini
[global]
index-url = https://pypi.org/simple
extra-index-url = https://pypi.jobtechdev.se/repository/pypi-jobtech/simple
```

Include this line in your Dockerfile:
`COPY pip.ini /etc/pip.conf`

If you are having problems locally, you might need an additional `pip.ini`,
see [pip documentation](https://pip.pypa.io/en/stable/topics/configuration/) for where pip looks for configuration and
in which order.

## Poetry

Some projects use [Poetry](https://python-poetry.org/docs/#installation) for dependency management. Poetry has to be
installed.

The main dependencies are specified in a file called `pyproject.toml`, from these specifications a file
called `poetry.lock` is created.

⚠️ Do not edit `poetry.lock`, this could lead to inconsistencies. The only allowed way to change that file is
with the command `poetry lock` (which creates a lock file from the specification).

### Poetry & Docker/Podman

In a Dockerfile, poetry is installed and the project dependencies are installed like this:

```shell
RUN python -m pip install --upgrade pip poetry &&\
    python -m poetry config virtualenvs.create false &&\
    python -m poetry install
```

### Additional dependencies

In some cases, there are additional groups in the poetry specification in `pyproject.toml`. This allows us to specify
all dependencies needed to _work_ with a project, but are not needed to _run_ it.

An example of additional groups:

```yaml
[ tool.poetry.group.pytest.dependencies ]
  # separate group for installation in Docker image
  pytest = "^7.4.3"

[ tool.poetry.group.dev.dependencies ]
  requests = "^2.31.0"
  pytest-cov = "^4.1.0"
  xmltodict = "^0.13.0"
  pre-commit = "^3.6.0"
  ruff = "^0.1.11"
```

In this case you would need to run `python -m poetry install --with pytest,dev` to get everything you need to do
development and testing, but these dependencies does not have to be installed in the image that actually runs the
product.
Special installation instructions must be included in the project documentation.

### Jobtech PyPi with Poetry

If you are using packages from Jobtech's PyPi, some configuration is needed in pyproject.toml.
If this causes problem, first check that you are using Poetry version > 1.6

```yaml
[ [ tool.poetry.source ] ]
  name = "pypijobtech"
  url = "https://pypi.jobtechdev.se/repository/pypi-jobtech/simple"
  priority = "supplemental"
```

## Formatting

If you are reformatting a repo, do it in a single commit and merge so that you don't have to try to see code changes in
combination with formatting. The diff will be huge.

Team Jobbdata is looking at [Ruff](https://astral.sh/ruff) for linting and formatting.
Suggested rule:

- line length 120
- Additional option "I" to sort imports

The formatter Black seems to have an issue where comments to disable specific linter checks can be moved by its formatting.

## Linting

Team Jobbdata is looking at [Ruff](https://astral.sh/ruff) for linting and formatting.
Use default rules until you are comfortable in adding stricter rules.

Ruff is fast and have reasonable default rules. Other linters like Pylint would need more configuration and disabling of checks.

## Testing

Use [Pytest](https://docs.pytest.org) as primary test runner, but in some cases it might be better to use Unittest (from
Python stdlib).
e.g. when you need to mock, or for very simple testing where you don't want to install additional dependencies.

## Configuration

Specify configuration in `pyproject.toml`, if possible, even if you don't use Poetry.
Any additional configuration files must be documented.
e.g.

```yaml
[ tool.ruff ]
  line-length = 120
```

## pre-commit
See separate instructions https://gitlab.com/arbetsformedlingen/documentation/-/blob/main/engineering/plattform/pre-commit-installation-configuration.md

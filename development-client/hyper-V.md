# Hyper-V in Windows

Use it to get a virtual GNU/Linux environment on your AF Windows laptop,
with a working graphical desktop.

This document is work in progress, and a place to write down tips on how to get it working.


## Configuring DNS in Ubuntu

If you created your machine with Hyper-V's "quick create", DNS will not be working out of the box.

Edit `/etc/systemd/resolved.conf`
and set `DNS=8.8.8.8` or to some other working DNS server.

Activate the settings: `sudo systemctl restart systemd-resolved`

Check that the DNS server is configured: `resolvectl status`


## Setting up a network switch that can reach both onprem and internet

This section is only a chaotic collection of notes from when I set it up on my laptop.

Unfortunately, I dare not to touch the settings to verify the procedure, as I'm not sure that I could get
it back up working again.

```
Windows PowerShell
Copyright (C) Microsoft Corporation. All rights reserved.
Try the new cross-platform PowerShell https://aka.ms/pscore6                                                                                                                                                                                    PS C:\WINDOWS\system32> New-VMSwitch -SwitchName "MyNatNetwork" -SwitchType Internal                                    
Name         SwitchType NetAdapterInterfaceDescription
----         ---------- ------------------------------
MyNatNetwork Internal


PS C:\WINDOWS\system32> get-netadapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Bluetooth-nätverksansl... Bluetooth Device (Personal Area Netw...      23 Disconnected 0C-9A-3C-41-49-1F         3 Mbps
VirtualBox Host-Only N... VirtualBox Host-Only Ethernet Adapter        21 Up           0A-00-27-00-00-15         1 Gbps
vEthernet (Default Swi... Hyper-V Virtual Ethernet Adapter             44 Up           00-15-5D-42-16-C0        10 Gbps
vEthernet (MyNatNetwork)  Hyper-V Virtual Ethernet Adapter #2          35 Up           00-15-5D-38-01-01        10 Gbps
Wi-Fi                     Intel(R) Wi-Fi 6 AX201 160MHz                12 Up           0C-9A-3C-41-49-1B       1.7 Gbps


PS C:\WINDOWS\system32> New-NetIPaddress -IPAddress 192.168.66.1 -PrefixLength 24 -InterfaceIndex 35


IPAddress         : 192.168.66.1
InterfaceIndex    : 35
InterfaceAlias    : vEthernet (MyNatNetwork)
AddressFamily     : IPv4
Type              : Unicast
PrefixLength      : 24
PrefixOrigin      : Manual
SuffixOrigin      : Manual
AddressState      : Tentative
ValidLifetime     : Infinite ([TimeSpan]::MaxValue)
PreferredLifetime : Infinite ([TimeSpan]::MaxValue)
SkipAsSource      : False
PolicyStore       : ActiveStore

IPAddress         : 192.168.66.1
InterfaceIndex    : 35
InterfaceAlias    : vEthernet (MyNatNetwork)
AddressFamily     : IPv4
Type              : Unicast
PrefixLength      : 24
PrefixOrigin      : Manual
SuffixOrigin      : Manual
AddressState      : Invalid
ValidLifetime     : Infinite ([TimeSpan]::MaxValue)
PreferredLifetime : Infinite ([TimeSpan]::MaxValue)
SkipAsSource      : False
PolicyStore       : PersistentStore



PS C:\WINDOWS\system32> New-NetNat -Name MyNATnetwork -InternalIPInterfaceAddressPrefix 192.168.66.0/24


Name                             : MyNATnetwork
ExternalIPInterfaceAddressPrefix :
InternalIPInterfaceAddressPrefix : 192.168.66.0/24
IcmpQueryTimeout                 : 30
TcpEstablishedConnectionTimeout  : 1800
TcpTransientConnectionTimeout    : 120
TcpFilteringBehavior             : AddressDependentFiltering
UdpFilteringBehavior             : AddressDependentFiltering
UdpIdleSessionTimeout            : 120
UdpInboundRefresh                : False
Store                            : Local
Active                           : True



PS C:\WINDOWS\system32>





New-VMSwitch -name ExternalSwitch  -NetAdapterName Ethernet -AllowManagementOS $true

https://www.google.com/search?client=firefox-b-d&q=new-vmswitch+%220x800700B7%22
https://blog.boris-wach.de/permalink/277
    (kontrollpanelen)
    Open Network Sharing Center (just to be prepared! You need to be kind of quick)
    Click ‘Change Adapter Settings’ in the left-hand menu (leave window open for later)
    Create the new External Switch in Hyper-V Manager. Or, you can also use New-VMSwitch PowerShell cmdlet. Both methods work.
    While the “Applying” dialog is spinning away, or the PowerShell Progress “dialog”, switch over to the network adapters dialog you have open in Control Panel. (now you need to be kind of quick)
    Disable your WiFi adapter
    Reenable your Wifi adapter
    Check in the network adapter settings if your external switch has been created
```

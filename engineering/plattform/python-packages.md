# Python paket

Vi kan publicera våra Python paket på [pypi.jobtechdev.se](https://pypi.jobtechdev.se).
Detta dokument beskriver vilka inställningar som behöver göras för att ladda upp paket
till våran PyPi. Hur man gör för att kunna skapa paket i Python beskrivs bra i
[Pythons dokuemntation](https://packaging.python.org/en/latest/tutorials/packaging-projects/)
och dupliceras därmed inte här.

Alla paket som publiceras där är åtkomliga för hela internet.
Däremot är det bara vi på JobTech som kan publicera paket.
Om ett paket med en specifik version har laddats upp så kan det inte skrivas över.
Samma paket men med nya versionsnummer går ladda upp.

## Ladda ner paket

För att kunna ladda ner paket från [pypi.jobtechdev.se](https://pypi.jobtechdev.se) samt
fortsätta ladda ner paket från centrala [pypi.org](https://pypi.org) så behöver du lägga in
följande konfigurationsfil.

Placera filen:

* Linux: `$HOME/.config/pip/pip.conf`
* Mac: `$HOME/Library/Application Support/pip/pip.conf` om katalogen
 `$HOME/Library/Application Support/pip` finns, annars `$HOME/.config/pip/pip.conf`
* Windows: `%APPDATA%\pip\pip.ini` **Notera:** Om den gamla `%HOME%\pip\pip.ini`
  finns så läses den också in, vilket kan ge konflikter

```ini
[global]
index-url = https://pypi.org/simple
extra-index-url = https://pypi.jobtechdev.se/repository/pypi-jobtech/simple
```

## Ladda upp paket

Detta varierar lite beroende på vilket verktyg man använder.
Detta utgår från paketering med build och twine för
uppladdning.

Installera paketen:

* build
* twine

Vi antar att `setup.py` eller `setup.cf` redan finns.

Sätt först miljövariablerna `TWINE_USERNAME` och `TWINE_PASSWORD`
till användarnamn och lösenord i LDAP.

Kör följande kommando för att bygga paketet och sedan
ladda upp det till repositoryt:

```shell
python3 -m build
python -m twine upload --repository-url https://pypi.jobtechdev.se/repository/pypi-jobtech/ *.whl *.tar.gz
```

Om du får behörighetsproblem att ladda upp till Nexus be Calamari att lägga till
rollen *developer* på din användare i Nexus.

Notera att samma version av ett paket kan inte laddas upp flera gånger.

## Använda CI/CD

### Konfigurera ett repository

Kontrollera att ditt projekt går bygga med `python3 -m build` från rooten av repositoryt.

I GitLab, gå till inställningarna för projektet och skapa en ny webhook.
Fyll i formuläret för att konfigurera webbhooken.

* Fältet *URL* skall ha värdet `https://aardvark-py-hook-v1.jobtechdev.se/`
* Fältet *Secret token* skall ha en hemlighet för att Aardvark skall acceptera
  anropet. Fråga Calamari vad aktuellt värde är eller kopiera från ett repository
  som redan har Aardvark uppsatt.
* Klicka i checkboxarna för:
  * Push events
  * Tag push events
  * Merge request events

Aardvark-py kommer nu att kommentera merge requests med en länk till
information om bygget. Det innehåller information om körda test, test-
täckning, utdaterade paket etc.

Du kan vilja konfigurera hur tester och kodtäckning körs.  Det är möjligt att
konfigurera om det per repository, [läs mer här](https://gitlab.com/arbetsformedlingen/devops/aardvark-py/-/blob/main/docs/settings.md)

### Göra en release

Kontrollera att du i setuptools (`setup.py` eller `setup.cf`) har satt en ny version
och att det är commitat och mergat till default branchen. Var noga med att följa
[Python standard för versioner](https://www.python.org/dev/peps/pep-0440/)

Tagga commiten med versionen. Om så önskas kan taggen prefixas med ett v.

Paketet kommer nu att byggas och laddas upp till vår [egen PyPi(Nexus)](https://nexus.jobtechdev.se/#browse/browse:pypi-jobtech).

Annonsera din release till berörda.

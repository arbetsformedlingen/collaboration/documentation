# Aardvark

## Innehållsförteckning

[[_TOC_]]

## Inledning

Aardvark är ett dogmatiskt bygg- och installationsverktyg och
tillhandahålls som en tjänst. Att Aardvark är en tjänst innebär att du inte
behöver installera själva Aardvark, utan den är gemensam för hela JobTech.
Detta dokument beskriver hur Aardvark användas inom JobTech.
Generell dokumentation av Aardvark finns i
[GitLab-repositoryt](https://gitlab.com/arbetsformedlingen/devops/aardvark).

Aardvark kan användas för att antingen bara bygga
containerbilder (container images, docker images)
eller för att bygga containerbilder och installera dessa
i en eller flera miljöer. Aardvark är dogmatisk i avseendet att den
förväntar sig att kod-repository och infrastrukturkod är utformat
på visst sätt, samt förväntar sig att releasehantering följer en
speciell process.

Aardvarks syfte är att ta bort/minska behovet av att bygga egna bygg-
och installationspipelines. För att låta Aardvark kunna
installera programvara måste man skriva Kubernetes-manifest och använda
[Kustomize](https://kustomize.io/).

Aardvark kan användas antingen för att bara bygga container images eller
för hantera hela bygg- och installationskedjan och även göra installation
efter bygget.

Det finns ett relaterat project till Aardvark för Python och skapa
Python moduler, detta är dokumenterat i [separat dokumentation](python-packages.md).

### Läsanvisningar

Beroende på vad du vill göra så kan du välja att läsa olika delar av dokumentet.
Nästa avdelning "[Krav för Aardvark](#krav-for-aardvark)" beskriver några övergripande
krav som Aardvark ställer på projekt. Det är bra att läsa för alla.

Om man skall arbeta med ett projekt som redan använder Aardvark och vill förstå hur
ny funktionalitet kan utvecklas i projektet och sedan lanseras rekommenderas
"[Från utveckling till produktion](#från-utveckling-till-produktion)" som beskriver hela
processen.

Om ett projekt som tidigare inte har använt Aardvark skall börja göra det,
beskrivs det i "[Använda Aardvark i ett nytt project](#använda-aardvark-i-ett-nytt-projekt)".
Skall man göra ändringar i hur installationen görs, är det lämpligt att läsa avsnittet också, men
hoppa över delar om att sätta upp webhooks i GitLab.

**Tips:** Det finns ett exempelprogram för Aardvark som kan användas som
utgångspunkt för nya projekt. Källkoden hittar du under
[Aardvark-demo](https://gitlab.com/arbetsformedlingen/devops/aardvark-demo)
och infrastruktur koden
[Aardvark-demo-infra](https://gitlab.com/arbetsformedlingen/devops/aardvark-demo-infra).
Andra projekt som använder Aardvark listats i
[slutet av dokumentet](#projekt-som-använder-aardvark).

I resterande del av dokumentet användes *my-app* som namnet
på programvaran vi vill bygga och driftsätta. Byt ut det mot namnet på din programvara.

### Hjälp och Support

Hur Kubernetes manifest och Kustomize skrivs är utanför syftet med detta dokument.
Läs då istället [Kubernetes](https://kubernetes.io/docs/) och [Kustomize](https://kustomize.io/)
egen dokumentation.

Om du har frågor eller funderingar, ställ en fråga i
[Mattermost](https://mattermost.jobtechdev.se/calamari/channels/dev-ops).

## Krav för Aardvark

Källkoden måste finnas i ett kodrepository på [GitLab](https://gitlab.com/).
I toppen på repositoryt skall det finnas en fil `Dockerfile`.
Container imagen måste kunna byggas utan att några miljövariabler eller
andra extraparametrar är satta vid bygget. Repositoryts standardgren
skall heta `master` eller `main`. Detta dokument använder
`main` då det är standard på nya repositoryn som skapas.

**Tips:** Begränsa till att använda bokstäverna a till z, siffrorna 0-9
samt minus tecken(-) i namnet på programvaran.

Om Aardvark skall installera programvaran så måste det
finnas ett infrastruktur repository i GitLab jämte repositoryt för
källkoden. Detta repository måste heta samma som källkodsrepositoryt men
med det extra suffixet `-infra`, exempelvis `my-app-infra`. Syftet med
infra-repositoryt är att beskriva hur installationen skall se ut.

I infrastrukturrepositoryt skall manifesten för Kubernetes finnas i form av
[Kustomize](https://kustomize.io) filer. Vi återkommer
senare hur dessa skall organiseras. Kustomize ger oss möjlighet att
installera programvaran i olika miljöer med varierande konfiguration.

Om installationsdelen av Aardvark används, installeras alltid senaste
committen i standardgrenen i utvecklingsmiljön(develop).

Installation i övriga miljöer styrs med git-taggar. Den commit som
man vill skall installeras i test skall ha git-taggen
`test`. Motsvarande för övriga miljöer. Följande miljöer/taggar stödjs:

* `test`
* `staging`
* `i1`
* `t2`
* `prod`
* `prod-stdby`
* `onprem`

**Tips:** När grenar skapas, undvik och namnge dem *prod*, *prod-stdby*,
*staging*,*test*, *i1*, *t2* eller *onprem*.

Du behöver ha tillgång till OpenShift klustret för test samt kommandoradsverktyget
oc([nerladdning här](https://console-openshift-console.test.services.jtech.se/command-line-tools))
installerat på din dator.

## Från utveckling till produktion

Innan vi går in på hur man sätter upp sitt projekt för att använda
Aardvark går vi igenom hur man arbetar i ett projekt som använder Aardvark.
Det är trots allt det vanligaste scenariot.

Scenariot är att vi vill utveckla en ny funktion för *my-app* med
namnet *my-feature*. Vi utvecklar på en funktions-gren innan
vi mergar till vår standardgren. När vi mergar installeras den nya
funktionen i vår gemensamma utvecklingsmiljö. Detta kan vi repetera flera
gånger och införa flera nya funktioner. När vi är nöjda installerar
vi vår nya version i en test-miljö och slutligen i produktion.

Vi har alltså tre olika installationer av vårt program:

* gemensam utvecklingsmiljö - Denna ändras ofta och innehåller senaste
  ändringarna gjorda mergade till standardgrenen.
* testmiljö - Inför en release till slutanvändarna har vi en mer driftliknande
  miljö där vi kan göra utförligare tester. Ibland kallas denna miljö för staging.
* produktionsmiljö - Den miljö som slutanvändarna använder programvaran i.

Hela flödet vi arbetar med illustreras  i
[denna presentation](https://docs.google.com/presentation/d/e/2PACX-1vRqEmkly2UA0oxrctfKYCaykAJpgscN2J7RPbhAs_rt1VX3PMZe8MM9jmvznqQ3sQRn57X8zR_p521d/pub?start=true&loop=false&delayms=3000).

### Utveckling

Se till att ha den senaste versionen av standardgrenen uthämtad.
Skapa en gren för din funktion. Namnge den *my-feature*.
Nu börjar vi skriva vår kod.

Efter vi har kommit en bit, committar vi koden och pushar den till
GitLab. Nu triggas automatiskt ett bygge. På så sätt kan du säkerställa
att det inte bara bygger på "din maskin". För enkelhetens skull så
skapar du också en Merge request i GitLab.

Nu har kollegorna möjlighet att kommentera koden. Aardvark bygger
container imagen och markerar om den har klarat att bygga eller ej.
Nedanstående bild visar hur det ser ut i Merge requesten medan bygget pågår.

[![Aardvark visar pågående bygge i GitLab](images/aardvark-building-in-gitlab.png)](images/aardvark-building-in-gitlab.png)

När de små cirklarna visar en bock är bygget klart. Du kan behöva
ladda om GitLab-sidan för att den skall uppdateras.

**Notera:** Det kan ta uppåt en minut innan bygget startar.

Om du klickar på cirkeln på höger sida kan du välja att gå till
*ci/aardvark*. Välj det alternativet och du kommer till Aardvarks bygge
av din commit. Där kan du se detaljer om bygget som loggar
och vilka steg som tagits.
Bilden nedan visar hur det ser ut när man bygger en gren.

[![Aardvark visar avslutat bygge av gren i OpenShift](images/aardvark-pipeline-build-branch.png)](images/aardvark-pipeline-build-branch.png)

**Notera:** Vissa steg i bygget ignoreras när vi bygger en gren
som inte är standardgrenen.

Bygget skapar en container image som heter
`docker-images.nexus.jobtechdev.se/my-app/my-app`. Den får två taggar,
första är namnet på din gren *my-feature* och andra är de första 7 tecknen
från commit-sha som identifierar committen som har byggts.

Gör vi ytterligare ändringar i koden och pushar till vår merge request
så uppdateras taggen *my-feature* att peka på den senaste byggda imagen,
samt att den får den nya commit-sha.

**Tips:** Du kan ladda ner container imagen med följande namn
`docker-images.nexus.jobtechdev.se/my-app/my-app:my-feature`. Tänk på
att använder du taggen för gren-namnet så cachas containerbilden.

När funktionen är färdig, granskad och gödkänd mergar vu deb till
`main`-grenen. Åter igen så
startar Aardvark ett bygge, detta görs då det i `main` kan finnas ändringar
som inte finns med i `my-feature`-grenen. (I git är en merge alltid en
commit.)

På repositoryts hemsidan i GitLab ser du din standardgren. Där kommer
en ruta upp som visar bygget på samma sätt som för merge requesten.

[![Aardvark visar pågående bygge i GitLab i standardgrenen](images/aardvark-building-in-gitlab-main.png)](images/aardvark-building-in-gitlab-main.png)

Vid bygge av standardgrenen får vi tre taggar på container imagen:

* `main` - då det är namnet på vår standardgren.
* commit-sha - på committen som mergen sker i.
* `latest` - det är en vanlig praxis att senaste committen på standardgrenen
  får den tagen.

Klickar vi på cirkeln för bygget och går vi till den detaljerade sidan
för bygget ser vi att denna gång utförs även de steg som hoppades över
när vi byggde `my-feature`-grenen.

[![Aardvark visar avslutat bygge av gren i OpenShift](images/aardvark-pipeline-build-branch.png)](images/aardvark-pipeline-build-branch.png)

När bygget av main är klart, gå till infra-repositoryt. Där ser du att
det har tillkommit en commit. Denna är gjord av Aardvark och ser ut som
bilden nedan.

[![Commit i infra-repository flr ny containerbild](images/aardvark-gitlab-infra-commit.png)](images/aardvark-gitlab-infra-commit.png)

Aardvark ändrar vilken tag som skall användas i vår gemensamma
utvecklingsmiljö. Om du tittar i git-loggen så har `newTag`-fältet
samma värde som den senaste committens id. Det är denna ändring leder
i sin tur till att Aardvark installerar den nya versionen.

Efter ett par minuter kan den nya funktionen utvärderas i den gemensamma
utvecklingsmiljön.

### Driftsätt i testmiljön

När en eller flera nya funktioner anses färdiga för release gör vi
en installation i vår testmiljö. Där kan vi utföra noggrannare tester
i en produktionsliknande miljö.

Releasen och installationen görs genom att sätta en tag i vårt
källkodrepository på den commit som vi vill releasa.

Enklast är att göra test-releasen i GitLabs webbgränssnitt och på sidan för
att hantera taggar. Den kan se ut som nedan.

[![GitLab tag-översikt](images/aardvark-gitlab-tags-overview.png)](images/aardvark-gitlab-tags-overview.png)

För att göra en test-release skall vi sätta taggen `test`. Denna tag talar om
att just denna commit skall vara installerad i testmiljön.

**Tips:** För att hålla reda på vilka tidigare testreleaser bör vi
också sätta en versionstag, exempelvis
`v1.1rc1`. Alla taggar som sätts på en commit kommer också hamna på motsvarande
container image.

Börja med att radera taggen `test`, det görs enklast genom att trycka på papperskorgen
jämte `test`-taggen.

Klicka sedan på *New tag* och skapa en ny tag som heter `test` och pekar på senaste committen
 i `main`-grenen.

 **Tips:** Vill du göra det på kommandoraden så finns det en
[beskrivning på hur man hanterar taggar i git](https://gitlab.com/arbetsformedlingen/devops/aardvark/-/blob/main/docs/tag-for-release.md).

 Aardvark ser nu att vi vill göra en release och uppdaterar infra-repositoryt med vilken commit som
 skall installeras i test, detta triggar sedan installationen av vår nya test-version.

 **Notera:** Inget nytt bygge sker. Den git-commit som vi markerar för
test-release är redan byggd i samband med mergen. Detta gör att vi
garanterat kör exakt samma binärer som vi gjorde i utvecklingsmiljön.

Utrullningen kan följas i ArgoCD som sköter själva utrullningen. ArgoCD
nås via OpenShift consolen. Uppe till höger i OpenShift consolen klickar
mN på fyrkanten med fyrkanter(3x3) och väljer ArgoCD. Då kommer du till
consolen för ArgoCD som synkar förändringar från git(infra
repositoryt). Om den frågar om inloggning,
välj att logga in med OpenShift och logga in som man gör till OpenShift.

[![Starta ArgoCD fron OpenShift consolen](images/argocd-from-openshift.png)](images/argocd-from-openshift.png)

För att lättare hitta din applikation I ArgoCD, välj ditt team under
project. I consolen ser du status för synkningen av
applikationen och kan också trigga en extra sync.

För den som vill se statusen på utrullningen från kommandoraden,
använd: `kubectl -n openshift-gitops get Application`
Har allt gått bra så bör du få ungefär följande utskrift:

```text
NAME                 SYNC STATUS   HEALTH STATUS
my-app-prod          Synced        Healthy
```

### Driftsätt i produktion

Driftsättning till produktion sker på samma sätt som för test men vi använder taggen `prod` istället för `test`. Följ i övrigt instruktionerna
för [driftsättining i testmiljö](#driftsätt-i-testmiljön).

### Driftsätt i on-prem

Driftsättning till on-prem sker på samma sätt som för test men vi använder taggen `onprem` istället för `test`. Följ i övrigt instruktionerna
för [driftsättining i testmiljö](#driftsätt-i-testmiljön).

## Använda Aardvark i ett nytt projekt

Tidigare har vi sett hur Aardvark används för att bygga och releasa ett
projekt som redan var konfigurerat med Aardvark.

För att ett projekt/repository skall använda sig av Aardvark måste vi
göra Aardvark medvetet om projektet. Det beskrivs i detta avsnitt.

Vi kommer gå igenom tre olika moment. Var noga med att föregående moment
fungerar innan du går vidare med nästa.

[Första momentet](#sätta-upp-automatiskt-bygge-av-programvara) är att sätt
upp automatiskt bygge av vårat projekts container imagen.
Resultatet är att vår container image blir publicerad i ett centralt
repository som kan nås från samtliga våra
OpenShift kluster och våra arbetsstationer.

Det [andra momentet](#sätta-upp-automatiskt-installation-av-programvara)
är att göra att vår containerbild installeras automatiskt i den gemensamma
utvecklingsmiljön. Utvecklingmiljön kommer alltid köra den senaste commiten
på standardgrenen i git.

Slutligen i
[tredje momentet](#sätt-upp-installations-till-test-och-produktion)
aktiverar vi installation till test och produktionsmiljöer.
Produktionsmiljöerna på AWS och inne på Arbetsförmedlingen hanteras på
samma sätt.

**Tips:** Om du migrerar ett existerande projekt, skapa nya
OpenShift-project för de automatiska installationerna.
Då kan allt testas ut ordentligt innan övergång till riktig produktion.
När allt funkar som det ska, radera de gamla OpenShift-projekten.

### Sätta upp automatiskt bygge av programvara

I detta steg utgår vi från vårt källkodsrepository och skall sätta upp att
varje gång det görs en ny commit, oavsett gren, byggs en ny container image.
Om det sätts nya taggar för release så taggas container imagesen med dessa.

#### Bygg container image

Börja med att se till att det finns en `Dockerfile` i toppen av dit
repository. Den skall gå att bygga med antingen `podman build .` eller
`docker build .`. Inga extra argument eller miljövariabler skall krävas.

Kontrollera också att din standardgren heter `main` eller `master` och
att `Dockerfile` finns i den.

**Tips:** Om du använder ett kompilerande språk, använd
[multi-stage build](https://docs.docker.com/develop/develop-images/multistage-build/)
för kompilera och utföra
enhetstester i ett steg och paketera leveransen i ett andra steg.

Vi vet nu att vi kan bygga vårt repository korrekt.

#### Sätt upp webhook

Nu skall vi få GitLab att informera Aardvark när ändringar skett i
repositoryt. Detta gör vi genom att konfigurera en webhook i GitLab.

Surfa till GitLab sidan för dit repository. Under rubriken *Settings*,
välj *Webhooks*.

[![GitLabs meny visar Settings och Webhooks](images/aardvark-gitlab-webhook-menu.png)](images/aardvark-gitlab-webhook-menu.png)

Fyll i formuläret för att konfigurera webbhooken.

* Fältet *URL* skall ha värdet `https://aardvark-hook-v1.jobtechdev.se/`
* Fältet *Secret token* är hemlighet för att Aardvark skall acceptera anropet. Fråga Calamari vad aktuellt värde är eller kopiera från ett repository som redan har Aardvark uppsatt.
* Klicka i checkboxarna för:
  * Push events
  * Tag push events
  * Merge request events

[![GitLabs formulär för att sätta upp webhook](images/aardvark-gitlab-webhook-setup.png)](images/aardvark-gitlab-webhook-setup.png)

Klicka på *Add webhook*.

Prova att skicka ett test-event. Du skall då se att Aardvark
startar ett bygge av ditt repository. Du hittar körandes byggen i
[OpenShift konsolen](https://console-openshift-console.test.services.jtech.se/k8s/ns/aardvark/tekton.dev~v1beta1~Pipeline/aardvark-builder-pipeline/Runs).

När bygget är färdigt hittar du din containerbild i
[Nexus](https://nexus.jobtechdev.se/#browse/browse:JobtechdevDockerRegistry).

Om du får problem, ställ en fråga i
[Mattermost](https://mattermost.jobtechdev.se/calamari/channels/dev-ops).

När bygget fungerar kan vi få till nästa steg och sätta upp installationen.

#### Sätta upp automatiskt installation av programvara

Innan du börjar med detta steg måste steget ovanför där container imagen
byggs fungera.

Om du redan har fungerande Kubernetesmanifest så kan du återanvända dem.
Har du tidigare använt OpenShifts webbgränssnitt för att installera
programvaran kan du ladda ner YAML-filer därifrån och
utgå från dem.

**OBS!** Spara inte hemligheter som lösenord och API-nycklar i git. Just
nu rekommenderar vi att du installerar alla dessa manuellt. Checka
gärna in mallar för dessa i dit git-repository och beskriv dem i
README-filen. Det underlättar när en ny miljö måste skapas. Detta är en
avvägning mellan säkerhet och bekvämlighet, där vi bedömer att hemligheter
ändras relativt sällan ändras.

Kontrollera att du har en aktuell version av `oc` och eller `kubectl`.
(Alla kommandon där det står `kubectl` kan det bytas ut mot `oc`.)

**Tips:** Utgå från
[exempel programmets infrastrukturkod](https://gitlab.com/arbetsformedlingen/devops/aardvark-demo-infra)
för att komma igång med att installera din programvara med hjälp av kod.
[Sist i detta dokument listas](#projekt-som-använder-aardvark) även andra projekt som använder Aardvark som kan användas för inspiration.

##### Skapa Kubernetesmanifest

Om du inte redan har gjort det, skapa ett infrastruktur-repository i
GitLab. Det skall heta samma sak
som källkods-repositoryt men ha suffixet `-infra` och standardgrenen
måste vara `main` eller `master`.

Aardvark använder idag Kustomize för att installera och konfigurera
programvaran. Kustomize gör det enkelt att ha en bas-installation som passar
många miljöer. Vi kan sedan lägga på ändringar ovanpå bas-konfiguration som
är unika för varje miljö, exempelvis för att prata med en annan databas.
Läs mer om Kustomize på [hemsidan](https://kustomize.io/).

Skapa en katalogstruktur enligt nedanstående figur. Enklast är att
kopiera den direkt från
[Aardvarks demoprojekt](https://gitlab.com/arbetsformedlingen/devops/aardvark-demo-infra).

```text
.
+--kustomize/
   +--base/
      +--kustomization.yaml
      +--Rest of yaml to deploy app.
   +--overlays/
      +--develop/
         +--kustomization.yaml
         +--other yaml might need
      +--test/
         +--kustomization.yaml
         +--other yaml might need
      +--prod/
         +--kustomization.yaml
         +--other yaml might need
```

Katalogen `base` innehåller de gemensamma Kubernetesmanifesten. Så mycket
som möjligt skall vara lika mellan de olika miljöerna. Har du manifest
att utgå ifrån
kan du kopiera dem hit. Tag bort referenser till vilket namnspace de
skall installeras i och ändra
konfigurationer som riskerar att påverka produktion. Syftet är att
exempelvis en utvecklare
ska kunna använda denna bas för att sätta upp en egen miljö i valfritt namespace.
Referenser till container image (`image`) skall referera till containerbilden
som byggdes ovan. Förslag är att använda taggen `latest`. Lista alla
filerna som ingår i installationen som `resources` i `kustomization.yaml`.

Kontrollera att du får förväntat resultat genom att köra
`kubectl kustomize kustomize/base`. Detta kommando visar konfigurationen
som kommer installeras. Ser denna bra ut bör det nu gå att installera
programvaran i ditt aktuella namespace(OpenShift refererar till detta som
projekt). Skapa ett personligt lab-namespace för detta med
`oc new-project my-namespace` där `my-namespace` är ett unikt namn för
ditt namespace, inkludera gärna din AF-signatur.
För att installera använder du kommandot `kubectl apply -k kustomize/base`.
Korrigera konfigurationen så att den fungerar korrekt.

Nu skall vi anpassa installationen för varje miljö vi installerar i. Börja
med develop, när den fungerar gå då vidare till test
och slutligen produktion.

Anpassningarna för de olika miljöerna finns i katalogen `overlays`, med en
underkatalog per miljö. Dessa kataloger
måste namnges enligt figuren med katalogstrukturen ovan. Det som finns i
katalogen `test` driftsätts när man taggar källkoden `test`, sedan motsvarande
för `prod`, `onprem` etc.

I den enklaste fallet räcker det med att endast ha en `kustomization.yaml`
i de olika overlaysen. Den minimala filen ser ut såhär:

```yaml
#cluster: test
bases:
  - ../../base
namespace: aardvark-demo-develop
images:
  - name: docker-images.jobtechdev.se/aardvark-demo/aardvark-demo:latest
    newName: docker-images.jobtechdev.se/aardvark-demo/aardvark-demo
    newTag: XXXXXXXX
```

Första raden `#cluster: test` är det enda som är speciellt för Aardvark.
Den markerar att denna miljö skall installeras på test-klustret.
Finns inte raden, kommer Aardvark ignorera miljön. Denna rad skall bara
finnas i kustomization filen för develop miljön.

Raden med namespace är till vilket namespace som installationen skall ske.
Använd ett namespace per miljö. En bra praxis är att namnge dem *programvara-miljö*,
exempelvis `my-app-develop`.

Slutligen finns `images`-sektionen. Det är med denna magin och kopplingen mot
bygget av containerbilden finns.
På `name` raden skrivs samma containerbild som vi gjort i vår *base*, det
markerar att vi vill ersätta den bilden. Taggen måste finnas med i `name`.
I `newName` anges enbart namnet på containerbilden utan taggen. Slutligen
i `newTag` kan vi skriva `XXXXXX`.
Detta `XXXXXX` byts ut av Aardvark när den bygger en ny container mot commit-sha.

Det är vanligt att miljöerna behöver sätta miljövariabler eller
konfigurationsfiler olika.
[Kustomize dokumentation](https://kubectl.docs.kubernetes.io/guides/config_management/secrets_configmaps/)
beskriver hur det kan göras.
[Aardvarks demoprojekt](https://gitlab.com/arbetsformedlingen/devops/aardvark-demo-infra)
har också exempel på detta.

För att se hur kubernetesmanifesten ser ut efter att ett overlay är pålagt kör:
`kubectl kustomize kustomize/overlays/develop`. Inget på klustret påverkas
av kommandot.

När du är nöjd, kontrollera så att du inte har några hemligheter med.
Committa sedan koden till standardgrenen.

Vi har nu satt upp hur vi vill installera vår programvara.

#### Behörigheter för Aardvark

Vi har Kubernetesmanifest som fungerar och beskriver hur vår programvara
skall installeras. Nu skall vi ge Aardvark rättighet
att installera och ändra dem samt se till att Aardvark får reda på när
ny installation skall ske.

Om du inte redan har gjort det, skapa de projekt/namespace i OpenShift
som du behöver. Följ instruktionen i [Create a new project](Create%20a %20new%20project.md).

Aardvark skall nu få rättigheten att installera i namespacet. Det görs
genom att skapa en roll i vårt namespace och ge Aardvark den rollen.
Vi har tagit fram
[ett manifest](https://gitlab.com/arbetsformedlingen/devops/aardvark/-/raw/main/docs/aardvark-deploy-bot-access-to-project.yaml)
som ger Aardvark tillgång till de vanligaste behörigheterna.
Med denna får Aardvark rätt att installera:

* configmaps
* deployments
* horizontalpodautocalers
* routes
* services
* serviceaccounts

Behöver programvaran resurser måste de adderas.
I exemplet finns även beskrivning hur du utökar för cronjob
persistentvolumeclaims och statefulsets. Om du behöver göra ändringar,
checka in filen i ditt infra-repository.

Ge Aardvark rättigheterna med
`kubectl apply -f https://gitlab.com/arbetsformedlingen/devops/aardvark/-/raw/main/docs/aardvark-deploy-bot-access-to-project.yaml`.
Du måste stå i det namespace/projekt som installationen skall ske till.

Om du inte redan ha gjort det, installera hemligheterna nu.

#### Webhook

Uppsättningen av webbhook till vårt infrastrukturrepository sker på samma
sätt som vi gjorde för bygget. Surfa till GitLab sidan för dit repository.
Under rubriken *Settings*, välj *Webhooks*.

[![GitLabs meny visar Settings och Webhooks](images/aardvark-gitlab-webhook-menu.png)](images/aardvark-gitlab-webhook-menu.png)

Fyll i formuläret för att konfigurera webbhooken.

* Fältet *URL* skall ha värdet `https://aardvark-hook-v1.jobtechdev.se/`
* Fältet *Secret token* skall ha en hemlighet för att Aardvark skall acceptera anropet. Fråga Calamari vad aktuellt värde är eller kopiera från ett repository som redan har Aardvark uppsatt. Det är samma hemlighet som för bygget.
* Klicka i checkboxen för:
  * Push events

[![GitLabs formulär för att sätta upp webhook](images/aardvark-gitlab-webhook-setup.png)](images/aardvark-gitlab-webhook-setup.png)

Klicka sedan på *Add webhook*.

#### Testa

Att testa att hela kedjan från ändring till installation av utvecklingsmiljö fungerar
smidigast genom att göra en lite ändring som triggar hela kedjan.

1. Gå till ditt källkodrepository och gör en obetydlig ändring. Exempelvis
  lägg till en tom rad i README. Committa den till standardgrenen och pusha
  till GitLab.
1. Inom någon minut bör du se att
  [OpenShift konsolen för byggjobb](https://console-openshift-console.test.services.jtech.se/k8s/ns/aardvark/tekton.dev~v1beta1~Pipeline/aardvark-builder-pipeline/Runs)
  visar ditt byggjobb. Du ser det på namnet på `pipelinerun`.
1. När bygget är färdigt, kontrollera att du hittar container imagen i
  [Nexus](https://nexus.jobtechdev.se/#browse/browse:JobtechdevDockerRegistry).
1. Verifiera att Aardvark har uppdaterat vilken image som skall användas
  för develop-miljön. Titta i infrastruktur-repositoryt, där skall du se
  en ny commit gjord av Builder Bot. Overlayen för develop skall värdet
  för fältet `newTag` uppdaterats. (Samma som ändringens commit id.)
1. Installationen av den nya versionen startar i och med den nya committen
   till infrastrukturrepositoryt. Du ser det i
  [OpenShift konsolen för installationsjobbet](https://console-openshift-console.test.services.jtech.se/k8s/ns/aardvark/tekton.dev~v1beta1~Pipeline/aardvark-deployer-pipeline).
  Verifiera att den slutförs utan fel.

1. Gå till ditt namespace i OpenShift konsolen, där skall servicen ha
   startat.

Om du får problem, ställ en fråga i
[Mattermost](https://mattermost.jobtechdev.se/calamari/channels/dev-ops).

När utvecklingsmiljön fungerar som förväntat kan vi gå vidare till att
sätta upp test och slutligen produktionsmiljön. Detta beskrivs i nästa
avsnitt.

### Sätt upp installations till test och produktion

I develop så kan man ibland manuellt vilja experimentera med installationen
utan att behöva gå via git. Detta beteende vill vi inte ha i test och
produktion. Vi använder därför ett verktyg som heter
[ArgoCD](https://argoproj.github.io/argo-cd/) för att installera i dessa.
ArgoCD kontrollerar att beskrivningen i git stämmer med Kubernetes.

Vi kommer att instruera ArgoCD att monitorera vårt infrastrukturrepository
efter ändringar och installera dessa.

Säkerställ att containerbilder byggs ordentligt och installeras för
development innan du fortsätter.

Nedanstående kommer du uprepa två eller tre gånger, först för test.
Sedan för prod och slutligen för standby.

Börja med att sätt tagen `test` i ditt källkodsrepository.

I infrastrukturrepositoryt, skapa en overlay för test på samma sätt
som gjordes för [develop](#skapa-kubernetesmanifest).

Kontrollera att resultatet blir det förväntade med
`kubectl kustomize kustomize/overlays/test`. Inget på klustret påverkas
av kommandot.

Om du inte redan har gjort det, skapa de projekt/namespace i OpenShift
som du behöver. Följ instruktionen i [Create a new project](Create%20a %20new%20project.md). Installera också nödvändiga secrets.

ArgoCD skall nu få rättigheter att installera i namespacet. Det görs
genom att skapa en roll i vårt namespace och ge ArgoCD den rollen.
Vi har tagit fram
[ett manifest](https://gitlab.com/arbetsformedlingen/devops/aardvark/-/raw/main/docs/argocd-deployer-access-to-project.yaml)
som ger ArgoCD tillgång till de vanligaste behörigheterna.
Med denna får ArgoCD rätt att installera:

* configmaps
* deployments
* horizontalpodautocalers
* routes
* services
* serviceaccounts

Behöver programvaran resurser måste de adderas.
I exemplet finns även beskrivning hur du utökar för cronjob,
persistentvolumeclaims och statefulsets. Om du behöver göra ändringar,
checka in filen i ditt infra-repository.

Ge ArgoCD rättigheterna genom att sätta en label på ditt namespace.
(Byt ut my-app-test mot namnet på ditt namespace/projekt.)
`oc label namespace my-app-test argocd.argoproj.io/managed-by=openshift-gitops`

Installera också hemligheter som behövs för din programvara.

Nu är det dags att informera ArgoCD att monitorera vårt infrastrukturrepository. Checka ut repositoryt [argocd-infra](https://gitlab.com/arbetsformedlingen/devops/argocd-infra) och skapa en branch.

I katalogen `kustomize/clusters/test` lägg till en fil som heter som
din service plus miljö, exempelvis `my-app-test`. Filen skall se ut
enligt nedan.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  # Name of your Application, suggestion is to use same as the project
  # created to avoid conflicts.
  name: my-app-test
  namespace: openshift-gitops
spec:
  destination:
    # This namespace shall be same as the OpenShift project you created.
    namespace: my-app-prod
    server: https://kubernetes.default.svc
  project: default
  source:
    # URL to the git repo to deploy. Of the two repos you are working with it shall be the infra.
    repoURL: https://gitlab.com/gitlab.com/arbetsformedlingen/my-app-infra.git
    # Path to the overlay to deploy
    path: kustomize/overlays/test
    # Branch to use, most often main or master.
    targetRevision: main
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
    # Do not let ArgoCD create namespace, it will not get the right permissions.
    - CreateNamespace=false
```

* På sjätte raden står `name: my-app-test`, byt ut my-app mot namnet på din programvara.
* På 11 raden står det `namespace: my-app-test`, byt namnet till det
  projekt/namespace du skapade i OpenShift ovan.
* På rad 13, ändra `default` till namnet på ditt team.
* På 16 raden står det `https://gitlab.com/gitlab.com/arbetsformedlingen/my-app-infra.git`
  , byt den mot en länk till ditt infrastrukturrepository. Använd
  git-länken.
* Om din standardgren heter `master`, byt ut 20 raden från
  `targetRevision: main` till `targetRevision: master`

Lägg till filen du skapade ovan till listan `resources:` i filen `kustomize/clusters/test/kustomization.yaml`.

Committa ändringen och skapa en merge request. Be sedan Calamari reviewa
ändringen genom att skriva i
[Calamaris Town square i Mattermost](https://mattermost.jobtechdev.se/calamari/channels/town-square
)

När ändringen är mergad aktiverar ArgoCD installationen av programvaran.

Uppe till höger i OpenShift consolen klickar på fyrkanten med fyrkanter(3x3) och väljer
ArgoCD så kommer man till UI för applikationen som synkar förändringar från git(infra
repositoryt) till prod och test-miljöer. Där kan man se status för synkningen av
applikationen och också trigga en extra sync om man behöver. Om den frågar om inloggning,
välj att logga in med OpenShift och logga in som man gör till OpenShift.

[![Starta ArgoCD fron OpenShift consolen](images/argocd-from-openshift.png)](images/argocd-from-openshift.png)

Med kommandoraden gör: `kubectl -n openshift-gitops get Application`
Har allt gått bra så bör du få ungefär följande utskrift:

```text
NAME                 SYNC STATUS   HEALTH STATUS
my-app-test          Synced        Healthy
```

Gör om konfigurationen för ArgoCD för produktion och standby. För produktion byt ut alla `test` mot `prod`, för standby `prod-stdby` och för on-prem `onprem`.

## Projekt som använder Aardvark

Här är en lista på några projekt som redan använder Aardvark.
Dessa kan vara bra för att inspireras och hitta lösningar

* [JobLinks search API](https://gitlab.com/arbetsformedlingen/job-ads?filter=joblinks)
* [JobSearch](https://gitlab.com/arbetsformedlingen/job-ads?filter=jobsearch-api)
  och [relaterade importörer](https://gitlab.com/arbetsformedlingen/job-ads?filter=jobsearch-importers)
* [data.jobtechdev.se](https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se) -
  Då projektet behöver flera container bilder så är projektet uppdelat i många
  git-repositoryn.
* [www.jobtechdev.se](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se) och
  dess [infra-repository](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-infra)
* [script-container](https://gitlab.com/arbetsformedlingen/devops/script-container) -
  Bygger en container bild som kan användas för felsökning etc. Ingen installation sker.

med flera...

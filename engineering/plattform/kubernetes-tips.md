# Kubernetes tips and tricks

This page contains a number of tips and tricks regarding Kubernetes.

## Table of Contents

[[_TOC_]]

## Documentation and reference links

* [RedHat OpenShift documentation](https://docs.openshift.com/container-platform/4.8/welcome/index.html)
* [Kubernetes API reference documentation](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.21)

## kubectl and oc

### Get version of client and server

You can get version of current `oc` and  `kubectl` running and of the kluster you are connected to.

```shell
$ oc version
Client Version: 4.7.19
Server Version: 4.8.25
Kubernetes Version: v1.21.6+b4b4813
```

```shell
$ kubectl version
Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.2", GitCommit:"8b5a19147530eaac9476b0ab82980b4088bbc1b2", GitTreeState:"clean", BuildDate:"2021-09-15T21:38:50Z", GoVersion:"go1.16.8", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"21", GitVersion:"v1.21.6+b4b4813", GitCommit:"cefce093e4e5bc9a1916eb5a489ed37c7d467f6f", GitTreeState:"clean", BuildDate:"2021-12-15T00:02:57Z", GoVersion:"go1.16.6", Compiler:"gc", Platform:"linux/amd64"}
```

As you see the `oc`-command percents OpenShift version and Kubernetes version on the server, but `kubectl` only talks Kubernetes version on both client and server. Most often, you are interested on the version of Kubernetes server to know what API it supports.

In the above examples, it is recommended to upgrade the `oc`-command so it is at least on the same OpenShift version as the server.

## Persistent storage

### Find pod using a PVC

Unfortunately the PVC does not contain metadata about who is using it. Instead all pod:s must be queried. The following command
get all pod:s and get json output. The Json is then parsed to find all pods containing PVC.

```shell
kubectl get pods --all-namespaces -o=json | jq -c '.items[] | {name: .metadata.name, namespace: .metadata.namespace, claimName: .spec |  select( has ("volumes") ).volumes[] | select( has ("persistentVolumeClaim") ).persistentVolumeClaim.claimName }'
```

## Routing and Networking

### Rate limit

Number of requests per IP to a route can be limited. This is done by setting two annotations on the route.
If your application has multiple routes, this must be set on each route.

Following is an example of a simple route limiting to 10 requests per IP and second.

```yaml
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  annotations:
    haproxy.router.openshift.io/rate-limit-connections: "TRUE"
    haproxy.router.openshift.io/rate-limit-connections.rate-http: "10"
  name: echo
  namespace: runmm-echo
spec:
  host: echo-runmm-echo.test.services.jtech.se
  tls:
    insecureEdgeTerminationPolicy: Redirect
    termination: edge
  to:
    kind: Service
    name: echo
    weight: 100
  wildcardPolicy: None
```

If you are not managing your configuration via manifest-files the two annotations can also be set using `oc`:

```shell
oc annotate route echo haproxy.router.openshift.io/rate-limit-connections=TRUE
oc annotate route echo haproxy.router.openshift.io/rate-limit-connections.rate-http=10
```

More details and other possible settings to limit traffic can be found in the
[OpenShift documentation](https://docs.openshift.com/container-platform/4.8/networking/routes/route-configuration.html#nw-route-specific-annotations_route-configuration).

## Secrets
This is JobTech's recommendation version 0. It will be further refined in the future.

### Directory structure
```
your-secrets-folder
├── prod
│   ├── kustomization.yaml
│   └── secrets.yaml
└── test
    ├── kustomization.yaml
    └── secrets.yaml
```

The contents of both `kustomization.yaml` is:
```
resources:
- secrets.yaml
```

### Adding a secret as an environment variable

Add the code below to your `secrets.yaml` files. The secret itself inserted in the code must be base64-encoded. This encoding can be done in several ways - one is presented in the comment (for linux).
```yaml
#produce a base64 encoded token like this:
#   echo -n 'this is a secret' | base64
apiVersion: v1
kind: Secret
metadata:
  name: example-secret-env
data:
  ENV_SECRET0: dGhpcyBpcyBhIHNlY3JldA==
  ENV_SECRET1: ...
```

### Adding a secret as a file in the application container
Add the code below to your `secrets.yaml` files. The example contains two files: `config` and `credentials`, with their respective contents.
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: example-secret-file
stringData:
  config: |-
    [default]
    region = eu-central-1
    output = json

  credentials: |-
    [default]
    aws_access_key_id = example-secret0
    aws_secret_access_key = example-secret1
```

### Configuring your infra project to use the secrets
Here is shown how to configure a container with both of the above secrets, making the environment variable `ENV_SECRET0` (automatically base64-decoded) and the files `/secrets/config` and `/secrets/credentials` available.
```yaml
    spec:
      containers:
      - name: example-container
        envFrom:
        - secretRef:
            name: example-secret-env
        volumeMounts:
        - name: secret-volume
          mountPath: /secrets
      volumes:
      - name: secret-volume
        secret:
          secretName: example-secret-file
```


### Deploying secrets
You can commit your secret repository to git, as long as you make sure it is set to private.

To deploy your secrets:
```
# first login to the correct cluster (test or prod). The login command can be copied from the OpenShift web console, in the drop down menu in the top right corner.
oc login ...

# login to your project:
oc project your-project

# apply either the test or prod, according to environment, by specifying 
# one of the two directories:
oc apply -k test (or prod)
```
